using System;
using BowlingKata;
using NUnit.Framework;

namespace BowlingKataTests
{
    public class GameTests
    {
        private Game _game;

        [SetUp]
        public void Setup()
        {
            _game = new Game();
        }

        [Test]
        public void Cant_roll_more_than_ten_pins()
        {
            Assert.Throws<ArgumentException>(() => _game.Roll(11));
        }

        [Test]
        public void Cant_Roll_less_than_zero()
        {
            Assert.Throws<ArgumentException>(() => _game.Roll(-1));
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(6)]
        [TestCase(7)]
        [TestCase(8)]
        [TestCase(9)]
        [TestCase(10)]
        public void Can_Roll_zero_to_ten_pins(int pins)
        {
            Assert.DoesNotThrow(() => _game.Roll(pins));
        }

        [Test]
        public void Can_calculate_score_after_two_rolls()
        {
            // Arrange
            _game.Roll(3);
            _game.Roll(5);

            // Act
            var score = _game.Score();

            // Assert
            Assert.AreEqual(8, score);
        }

        [Test]
        public void Cant_Roll_More_Than_Ten_Pins_In_First_Frame()
        {
            // Arrange
            _game.Roll(9);

            // Assert
            Assert.Throws<ArgumentException>(() => _game.Roll(6));
        }

        [Test]
        public void Cant_Roll_More_Than_Ten_Pins_In_A_Normal_Frame()
        {
            // Arrange
            _game.Roll(6);
            _game.Roll(3);
            _game.Roll(9);

            // Assert
            Assert.Throws<ArgumentException>(() => _game.Roll(6));
        }

        [Test]
        public void Test_For_Four_Rolls_CheckValid_Score()
        {
            //Arrange
            _game.Roll(3);
            _game.Roll(4);
            _game.Roll(6);
            _game.Roll(0);

            // Act
            var score = _game.Score();

            // Assert
            Assert.AreEqual(13, score);

        }

        [Test]
        public void Spare_Is_Scored_Including_Next_Roll()
        {
            //Arrange
            _game.Roll(9);
            _game.Roll(1);
            _game.Roll(9);
            _game.Roll(0);

            // Act
            var score = _game.Score();

            // Assert
            Assert.AreEqual(28, score);
        }

        [Test]
        public void Strike_Is_Scored_Including_Next_Two_Rolls()
        {
            //Arrange
            _game.Roll(10);
            _game.Roll(5);
            _game.Roll(4);

            // Act
            var score = _game.Score();

            // Assert
            Assert.AreEqual(28, score);
        }

        [Test]
        public void Can_only_score_a_full_game()
        {
            // Arrange
            _game.Roll(1);
            _game.Roll(1);

            // Act & Assert
            Assert.Throws<Exception>(() => _game.Score());
        }

        [Test]
        public void Can_score_a_full_game()
        {
            // Arrange
            RollFullGame();

            // Act & Assert
            Assert.DoesNotThrow(() => _game.Score());
        }

        private void RollFullGame()
        {
            for (int i = 0; i < 20; i++)
            {
                _game.Roll(1);
            }
        }
    }
}
