﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace BowlingKata
{
    public class Game
    {
        private readonly List<Frame> _allFrames;
        private Frame _currentFrame;


        public Game()
        {
            _currentFrame = new Frame();
            _allFrames = new List<Frame>();
        }

        public void Roll(int pins)
        {
            _currentFrame.Roll(pins);
            if (_currentFrame.IsComplete)
            {
                _allFrames.Add((_currentFrame));
                _currentFrame = new Frame();

            }
        }

        public int Score()
        {
            if (_allFrames.Count != 10)
            {
                throw new Exception("Can only score a full game");
            }

            var score = 0;

            for (var i = 0; i < _allFrames.Count; i++)
            {
                var frame = _allFrames[i];
                var frameIsLastFrame = i == _allFrames.Count - 1;
                var frameIsSecondLastFrame = i == _allFrames.Count - 2;

                score += frame.TotalPins;

                if (frame.IsStrike && !frameIsLastFrame)
                {
                    if (_allFrames[i + 1].IsStrike)
                    {
                        score += _allFrames[i + 1].TotalPins;
                        if (!frameIsSecondLastFrame)
                        {
                            score += _allFrames[i + 2].RollOne ?? 0;
                        }
                    }
                    else
                    {
                        score += _allFrames[i + 1].TotalPins;
                    }
                }

                if (frame.IsSpare && !frameIsLastFrame)
                {
                    score += _allFrames[i + 1].RollOne ?? 0;
                }
            }

            return score;
        }
    }
}
