﻿using System;

namespace BowlingKata
{
    public class Frame
    {
        public int? RollOne { get; set; }
        public int? RollTwo { get; set; }
        public int TotalPins => (RollOne ?? 0) + (RollTwo ?? 0);
        public bool IsComplete { get; set; }

        public bool IsStrike => IsComplete && (RollOne ?? 0) == 10;
        public bool IsSpare
        {
            get
            {
                if (!IsComplete || IsStrike)
                {
                    return false;
                }

                var total = RollOne ?? 0;

                if (RollTwo.HasValue)
                {
                    total += RollTwo.Value;
                }

                return total == 10;
            }
        }

        public void Roll(int pins)
        {
            if (pins > 10)
            {
                throw new ArgumentException("Too many pins");
            }

            if (pins < 0)
            {
                throw new ArgumentException("Too few pins");
            }

            if (!RollOne.HasValue)
            {
                RollOne = pins;
                if (pins.Equals(10))
                {
                    IsComplete = true;
                }
                return;
            }

            if (!RollTwo.HasValue)
            {
                if (RollOne + pins > 10)
                {
                    throw new ArgumentException("Cannot roll more than 10 pins in one frame");
                }

                RollTwo = pins;
                IsComplete = true;
            }
        }
    }
}
